<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Auth;

class DashboardController extends BaseController
{
    
    public function __construct()
    {
        // parent::__construct();
        $this->middleware('auth');
    }
    
    function index(){
        // $user = Auth::user();
        // dd($user);

    	return view('dashboard.index');
    }
}
