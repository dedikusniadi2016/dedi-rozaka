<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MasterController extends Controller
{
    function employees() {
        return view('project.ticket-list');
    }

    function outlets() {
        return view('outlets.outlet-list');
    }
}
