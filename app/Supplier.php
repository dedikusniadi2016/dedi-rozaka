<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Supplier extends Model
{
    protected $table = 'tbl_suppliers';
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'name', 'contact_person', 'phone', 'email', 'address', 'description', 'user_id', 'del_status',
    ];

    protected $hidden = [
        'user_id', 'del_status', 
    ];

    
    //scope for order
    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

}
