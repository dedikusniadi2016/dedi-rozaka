<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Ingredient;

class Unit extends Model
{
    protected $table = 'tbl_units';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = [
        'id', 'unit_name', 'description', 'del_status',
    ];

    public function ingredients()
    {
        return $this->hasMany(Ingredient::class, 'unit_id');
    }
}
