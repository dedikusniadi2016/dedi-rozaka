<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Ingcat;
use App\Unit;

class Ingredient extends Model
{
    protected $table = "tbl_ingredients";
    protected $primaryKey = "id";
    public $incrementing = false;

    protected $fillable = [
        'id', 'code', 'name', 'category_id', 'purchase_price', 'alert_quantity', 'unit_id', 'user_id', 'del_status'
    ];

    protected $hidden = [
        'user_id', 'del_status', 
    ];

    public function scopeLatestFirst($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function ingcat()
    {
        return $this->belongsTo(Ingcat::class, 'category_id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }
}
