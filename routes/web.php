<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {return view('welcome');});
Route::get('/', function () { return redirect('dashboard/index'); });

// auth routes
Auth::routes();

/* Dashboard */
Route::get('dashboard', function () { return redirect('dashboard/index'); });
Route::get('dashboard/index', 'DashboardController@index')->name('dashboard.index');

/* Profile */
Route::get('profile', function () { return redirect('profile/my-profile'); });
Route::get('profile/my-profile', 'ProfileController@myProfile')->name('profile.my-profile');

/* Master */
Route::get('master', function() { return redirect('master/outlets'); });
Route::get('master/employees', 'MasterController@employees')->name('master.employees');

/* Outlet */
// Route::get('outlets', function () { return redirect('outlets'); });
Route::get('outlets', 'OutletController@index')->name('outlets');
Route::get('outlets/create', 'OutletController@create')->name('outlets.create');
Route::get('outlets/edit/{outlet}', 'OutletController@edit')->name('outlets.edit');
Route::post('outlets', 'OutletController@store')->name('outlets.store');
Route::put('outlets/{outlet}', 'OutletController@update')->name('outlets.update');
// Route::resource('outlets', 'OutletController');

/* Employees */
// Route::get('employees', function() { return redirect('employees'); });
Route::get('employees', 'EmployeeController@index')->name('employees');
Route::get('employees/create', 'EmployeeController@create')->name('employees.create');
Route::get('employees/edit/{employee}', 'EmployeeController@edit')->name('employees.edit');
Route::post('employees', 'EmployeeController@store')->name('employees.store');
Route::put('employees/{employee}', 'EmployeeController@update')->name('employees.update');

/* Category Ingredient */
Route::get('ingredient', function() { return redirect('ingcats'); });
Route::get('ingredient/ingcats', 'IngcatController@index')->name('ingcats');
Route::get('ingredient/ingcats/create', 'IngcatController@create')->name('ingcats.create');
Route::get('ingredient/ingcats/edit/{ingcat}', 'IngcatController@edit')->name('ingcats.edit');
Route::post('ingredient/ingcats', 'IngcatController@store')->name('ingcats.store');
Route::put('ingredient/ingcats/{ingcat}', 'IngcatController@update')->name('ingcats.update');
/* Ingredient */
Route::get('ingredient/ingredients', 'IngredientController@index')->name('ingredients');
Route::get('ingredient/ingredients/create', 'IngredientController@create')->name('ingredients.create');
Route::get('ingredient/ingredients/edit/{ingredient}', 'IngredientController@edit')->name('ingredients.edit');
Route::post('ingredient/ingredients', 'IngredientController@store')->name('ingredients.store');
Route::put('ingredient/ingredients/{ingredient}', 'IngredientController@update')->name('ingredients.update');

/* Foods Management */
Route::get('food', function() { return redirect('food/maincategory'); });
Route::get('food/maincategory', 'FoodmCategoryController@index')->name('foodm');
Route::get('food/maincategory/create', 'FoodmCategoryController@create')->name('foodm.create');
Route::get('food/maincategory/edit/{foodmcategory}', 'FoodmCategoryController@edit')->name('foodm.edit');
Route::post('food/maincategory', 'FoodmCategoryController@store')->name('foodm.store');
Route::put('food/maincategory/{foodmcategory}', 'FoodmCategoryController@update')->name('foodm.update');
/* Foods Category */
Route::get('food/category', 'FoodCategoryController@index')->name('foodc');
Route::get('food/category/create', 'FoodCategoryController@create')->name('foodc.create');
Route::get('food/category/edit/{foodcategory}', 'FoodCategoryController@edit')->name('foodc.edit');
Route::post('food/category', 'FoodCategoryController@store')->name('foodc.store');
Route::put('food/category/{foodcategory}', 'FoodCategoryController@update')->name('foodc.update');

// supplier
Route::get('suppliers', 'SuppliersController@index')->name('suppliers');
Route::get('suppliers/create', 'SuppliersController@create')->name('suppliers.create');
Route::get('suppliers/edit/{suppliers}', 'SuppliersController@edit')->name('suppliers.edit');

Route::post('suppliers', 'SuppliersController@store')->name('suppliers.store');
Route::put('suppliers/{supplier}', 'SuppliersController@update')->name('suppliers.update');



